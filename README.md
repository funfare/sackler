The Sackler Family
=====

### Overview

This extension plays Lil Wyte's "Oxycotton" on any webpage that has the word Sackler in it.

### Installation

Clone this repo and load the entire folder as an unpacked chrome or firefox extension.

### How to help

[Direct Relief](https://www.directrelief.org/issue/opioid-epidemic/)

*In response to the opioid epidemic in the United States, Direct Relief is working with Pfizer to make up to 1 million doses of overdose-reversing Naloxone available at no cost to community health centers, free and charitable clinics, public health departments and other nonprofit providers nationwide. Naloxone can rapidly revive normal breathing in an individual who has overdosed on heroin or prescription opioid medications.*


